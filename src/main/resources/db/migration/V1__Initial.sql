CREATE TABLE filter (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE criterion (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    type VARCHAR(255) NOT NULL,
    condition VARCHAR(255) NOT NULL,
    criterion_value VARCHAR(255) NOT NULL
);

CREATE TABLE filter_criteria (
    filter_id INT NOT NULL,
    criterion_id INT NOT NULL,
    PRIMARY KEY (filter_id, criterion_id),
    FOREIGN KEY (filter_id) REFERENCES filter(id),
    FOREIGN KEY (criterion_id) REFERENCES criterion(id)
);

INSERT INTO filter (id, name)
VALUES (100, 'filter1'),
       (101, 'filter2');

INSERT INTO criterion (id, type, condition, criterion_value)
VALUES (100, 'Amount', 'More than', '10'),
       (101, 'Title', 'Contains', 'hei'),
       (102, 'Title', 'Starts with', 'yes');

INSERT INTO filter_criteria (filter_id, criterion_id)
VALUES (100, 100),
       (100, 101),
       (101, 102);
