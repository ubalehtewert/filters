package com.example.filters.service;

import com.example.filters.entity.Filter;
import com.example.filters.entity.dto.FilterDTO;
import com.example.filters.entity.mapper.FilterMapper;
import com.example.filters.exception.FilterNotFoundException;
import com.example.filters.repository.FilterRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class FilterService {

    private final FilterRepository filterRepository;

    private final FilterMapper filterMapper;

    public List<FilterDTO> getFilters() {
        return filterMapper.toDtoList(filterRepository.findAll());
    }

    public FilterDTO saveFilter(FilterDTO filterDTO) {
        Filter dbFilter = filterRepository.save(filterMapper.toEntity(filterDTO));
        return filterMapper.toDTO(dbFilter);
    }

    public FilterDTO updateFilter(FilterDTO filterDTO) {
        if (filterRepository.existsById(filterDTO.id())) {
            Filter dbFilter = filterRepository.save(filterMapper.toEntity(filterDTO));
            return filterMapper.toDTO(dbFilter);
        }
        throw new FilterNotFoundException(filterDTO.id());
    }

    public void deleteFilter(int id) {
        if (!filterRepository.existsById(id)) {
            throw new FilterNotFoundException(id);
        }
        filterRepository.deleteById(id);
    }
}
