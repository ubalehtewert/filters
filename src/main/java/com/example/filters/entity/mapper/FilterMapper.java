package com.example.filters.entity.mapper;

import com.example.filters.entity.Filter;
import com.example.filters.entity.dto.FilterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)

public interface FilterMapper {

    Filter toEntity(FilterDTO filterDTO);

    FilterDTO toDTO(Filter filter);

    List<FilterDTO> toDtoList(List<Filter> filters);
}
