package com.example.filters.entity.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;

import java.util.List;

@Builder
public record FilterDTO(int id, String name, @NotEmpty(message = "You must add at least one criterion")  List<CriterionDTO> criteria) {}
