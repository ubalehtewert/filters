package com.example.filters.entity.dto;

import lombok.Builder;

@Builder
public record CriterionDTO(int id, String type, String condition, String value) {
}
