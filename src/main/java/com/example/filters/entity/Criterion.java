package com.example.filters.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "criterion")
public class Criterion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "You must insert a type to continue")
    @Size(min = 1, max = 50, message = "Type must be at least 1 characters and at most 50 characters")
    private String type;

    @NotBlank(message = "You must insert a condition to continue")
    @Size(min = 1, max = 50, message = "Condition must be at least 1 characters and at most 50 characters")
    private String condition;

    @Column(name = "criterion_value")
    @NotBlank(message = "You must insert a value to continue")
    @Size(min = 1, max = 50, message = "Value must be at least 1 characters and at most 50 characters")
    private String value;
}
