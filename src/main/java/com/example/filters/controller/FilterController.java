package com.example.filters.controller;

import com.example.filters.entity.dto.FilterDTO;
import com.example.filters.service.FilterService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/filters")
public class FilterController {

    private final FilterService filterService;

    @GetMapping
    public List<FilterDTO> getFilters() {
        return filterService.getFilters();
    }

    @PostMapping
    public ResponseEntity<FilterDTO> saveFilter(@Valid @RequestBody FilterDTO filterDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(filterService.saveFilter(filterDTO));
    }

    @PutMapping
    public FilterDTO updateFilter(@Valid @RequestBody FilterDTO filterDTO) {
        return filterService.updateFilter(filterDTO);
    }

    @DeleteMapping("/{id}")
    public Integer deleteFilter(@PathVariable int id) {
        filterService.deleteFilter(id);
        return id;
    }
}
