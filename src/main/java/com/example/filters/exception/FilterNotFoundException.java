package com.example.filters.exception;

public class FilterNotFoundException extends RuntimeException {

    public FilterNotFoundException(int id) {
        super("Filter with ID: " + id + " was not found!");
    }
}
